'use strict';

namespace App.Bootstrap.Init{
	let LoadRoutes = require('./routes/frontend/appRoutes'),
		LoaderVar  = LoadRoutes.LoaderModule.Loader;
	
	export class __BootStrap{
		constructor() {	
			LoaderVar.app.use(LoaderVar.express.static(__dirname + '/assets'));
			LoaderVar.app.set("view engine", "ejs");
			LoaderVar.app.set("views", LoaderVar.path.join(__dirname, 'views'));
			LoaderVar.app.listen(process.env.PORT || 5000);
			console.log('App runing...');
		}
	}
}

import Boot = App.Bootstrap;
let BootstapApp = new Boot.Init.__BootStrap();

