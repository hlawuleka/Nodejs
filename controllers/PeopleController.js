"use strict";
var PeopleController = (function () {
    function PeopleController() {
        this.Loader = require('../classes/loader');
    }
    PeopleController.prototype.setFirstName = function (firstName) {
        this.firstName = firstName;
    };
    PeopleController.prototype.setLastName = function (lastName) {
        this.lastName = lastName;
    };
    PeopleController.prototype.setBio = function (bio) {
        this.bio = bio || 'N/A';
    };
    PeopleController.prototype.setQueryOption = function (option) {
        this.queryOption = option;
    };
    PeopleController.prototype.getFirstName = function () {
        return this.firstName;
    };
    PeopleController.prototype.getLastName = function () {
        return this.lastName;
    };
    PeopleController.prototype.getBio = function () {
        return this.bio || null;
    };
    PeopleController.prototype.getQueryOption = function () {
        return this.queryOption;
    };
    PeopleController.prototype.getPeople = function (filterId, res) {
        this.setQueryOption("WHERE id " + (filterId != '' ? ' = ' + filterId : '!=0') + "");
        var query = "SELECT id, firstName, lastName, bio FROM sampleTable " + (this.getQueryOption()) + " ORDER BY id ASC";
        this.Loader.connection.query(query, function (error, rows, fields) {
            if (!!error) {
                return;
            }
            else {
                res.render("names", {
                    pageTitle: 'Person ' + (rows.length <= 0 ? ' Unkown ' : rows[0].firstName) + ' | App Page',
                    people: rows.length <= 0 ? [{ 'firstName': 'Oops, invalid account requested!', 'lastName': '', 'bio': '' }] : rows,
                    person: rows.length <= 0 ? false : (rows.length > 1 ? false : true)
                });
            }
        });
    };
    return PeopleController;
}());
var PeopleControllerInst = new PeopleController();
exports.PeopleControllerInst = PeopleControllerInst;
