"use strict";
var loadModules = (function () {
    function loadModules() {
        this.express      = require('express');
        this.mysql        = require('mysql');
        this.app          = this.express();
        this.path         = require('path');
        this.dbConnection = require('./dbConn');
    }
    return loadModules;
}());
var Loader = new loadModules();
exports.Loader = Loader;
var connectionProperties = Loader.dbConnection.connection;
exports.connectionProperties = connectionProperties;
var connection = Loader.mysql.createConnection({
    host: connectionProperties.host,
    user: connectionProperties.username,
    password: connectionProperties.password,
    database: connectionProperties.dbname
});
exports.connection = connection;
connection.connect(function (error) {
    if (!!error) {
        console.log("Error in connection!");
    }
});
