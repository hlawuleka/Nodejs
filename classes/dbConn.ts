import defineDBConstants  from "./constants/db__CONSTANTS__";
interface IDBProps {
	host : string;
	dbname : string;
	username : string;
	password : string;
}
class _DB_Connection implements IDBProps{
	
	host   : string;
	dbname : string;
	username : string;
	password   : string;
	
	constructor(dbProps: IDBProps) {
		this.host   = dbProps.host;
		this.dbname = dbProps.dbname;
		this.username = dbProps.username;
		this.password   = dbProps.password;
	}
}
class _connectionInstance extends _DB_Connection {
	
	constructor(dbProps : IDBProps){
		super(dbProps);
	}
}

let configVars = new defineDBConstants();
let connection = new _connectionInstance({

	host : configVars.host,
	dbname : configVars.dbname,
	username : configVars.username,
	password: configVars.password
});

export { connection }