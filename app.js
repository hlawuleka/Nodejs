'use strict';
var App;
(function (App) {
    var Bootstrap;
    (function (Bootstrap) {
        var Init;
        (function (Init) {
            var LoadRoutes = require('./routes/frontend/appRoutes'), LoaderVar = LoadRoutes.LoaderModule.Loader;
            var __BootStrap = (function () {
                function __BootStrap() {
                    LoaderVar.app.use(LoaderVar.express.static(__dirname + '/assets'));
                    LoaderVar.app.set("view engine", "ejs");
                    LoaderVar.app.set("views", LoaderVar.path.join(__dirname, 'views'));
                    LoaderVar.app.listen(process.env.PORT || 5000);
                    console.log('App runing...');
                }
                return __BootStrap;
            }());
            Init.__BootStrap = __BootStrap;
        })(Init = Bootstrap.Init || (Bootstrap.Init = {}));
    })(Bootstrap = App.Bootstrap || (App.Bootstrap = {}));
})(App || (App = {}));
var Boot = App.Bootstrap;
var BootstapApp = new Boot.Init.__BootStrap();
