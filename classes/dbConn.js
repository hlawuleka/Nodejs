"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var db__CONSTANTS__1 = require("./constants/db__CONSTANTS__");
var _DB_Connection = (function () {
    function _DB_Connection(dbProps) {
        this.host = dbProps.host;
        this.dbname = dbProps.dbname;
        this.username = dbProps.username;
        this.password = dbProps.password;
    }
    return _DB_Connection;
}());
var _connectionInstance = (function (_super) {
    __extends(_connectionInstance, _super);
    function _connectionInstance(dbProps) {
        _super.call(this, dbProps);
    }
    return _connectionInstance;
}(_DB_Connection));
var configVars = new db__CONSTANTS__1["default"]();
var connection = new _connectionInstance({
    host: configVars.host,
    dbname: configVars.dbname,
    username: configVars.username,
    password: configVars.password
});
exports.connection = connection;
