let  LoaderModule = require('../../classes/loader');
let PeopleController = require( '../../controllers/PeopleController');
let People = PeopleController.PeopleControllerInst;

LoaderModule.Loader.app.get('/', (req, res) => {
	res.render("index", {
		pageTitle : 'Welcome | App Page',
		data : null
	});
});
LoaderModule.Loader.app.get('/person/:id', (req, res) => {
	People.getPeople(req.params.id, res);
});
LoaderModule.Loader.app.get('/names', (req, res) => {
	People.getPeople('', res);
});

export {LoaderModule}
