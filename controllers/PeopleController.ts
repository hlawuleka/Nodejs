interface I_People{
	firstName : string;
	lastName : string;
	bio : any;
}
class PeopleController implements I_People{
	public firstName;
	public lastName;
	public bio: any;

	private queryOption;
	public Loader;
	private results;

	constructor(){
		this.Loader =  require('../classes/loader');
	}
	setFirstName(firstName:string){
		this.firstName = firstName;
	}
	setLastName(lastName:string){
		this.lastName = lastName;
	}
	setBio(bio?:any){
		this.bio = bio || 'N/A';
	}
	setQueryOption (option : any) {
		this.queryOption = option;
	}
	getFirstName() : string {
		return this.firstName;
	}
	getLastName() : string {
		return this.lastName;
	}
	getBio() : any {
		return this.bio || null;
	}
	getQueryOption () {
		return this.queryOption;
	}
	getPeople( filterId  , res) {

		this.setQueryOption("WHERE id " +( filterId != '' ? ' = '+ filterId  : '!=0'  ) +"");

		let query = "SELECT id, firstName, lastName, bio FROM sampleTable "+(this.getQueryOption())+" ORDER BY id ASC";
		this.Loader.connection.query(query, function(error, rows, fields){
			if(!!error){
				return;
				//some error logging
			}else{
				res.render("names", {
					pageTitle : 'Person ' + (rows.length <= 0 ? ' Unkown ' : rows[0].firstName)+' | App Page',
					people: rows.length <= 0 ? [{'firstName' : 'Oops, invalid account requested!', 'lastName' : '' , 'bio':''}] : rows,
					person: rows.length <= 0 ? false : ( rows.length > 1 ? false : true )
				});
			}	
		});
	}
}
let PeopleControllerInst = new PeopleController();
export {PeopleControllerInst} 

