"use strict";
var LoaderModule = require('../../classes/loader');
exports.LoaderModule = LoaderModule;
var PeopleController = require('../../controllers/PeopleController');
var People = PeopleController.PeopleControllerInst;
LoaderModule.Loader.app.get('/', function (req, res) {
    res.render("index", {
        pageTitle: 'Welcome | App Page',
        data: null
    });
});
LoaderModule.Loader.app.get('/person/:id', function (req, res) {
    People.getPeople(req.params.id, res);
});
LoaderModule.Loader.app.get('/names', function (req, res) {
    People.getPeople('', res);
});
