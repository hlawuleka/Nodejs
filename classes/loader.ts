interface I_LoadModules{
	express;
	mysql;
	app;
	path;
	dbConnection;
}
class loadModules implements I_LoadModules{
	public express;
	public mysql;
	public app;
	public path;
	public dbConnection;
	
	constructor(){
		this.express = require('express');
		this.mysql = require('mysql');
		this.app = this.express();
		this.path = require('path');
		this.dbConnection = require('./dbConn');		
	}
}

let Loader = new loadModules();
let connectionProperties = Loader.dbConnection.connection;
let connection = Loader.mysql.createConnection({
	host: connectionProperties.host,
	user: connectionProperties.username,
	password: connectionProperties.password,
	database: connectionProperties.dbname
});

connection.connect((error) => {
	if(!!error){
		console.log("Error in connection!");
	}
});

export {Loader, connection, connectionProperties}
